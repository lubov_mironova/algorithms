package ru.mla.insertionSort;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Класс для представления работы алгоритма сортировки вставками
 *
 * @author Mironova L.
 */
public class InsertionSort {
    public static void main(String[] args) {
        int[] selection = new int[9];

        for (int i = 0; i < selection.length; i++) {
            selection[i] = (int) (Math.random() * 10);
        }
        System.out.println("Сортировка массива:");
        System.out.println(Arrays.toString(selection));
        inserting(selection);
        System.out.println(Arrays.toString(selection));
        System.out.println();

        ArrayList<Integer> selectionList = new ArrayList<>();
        for (int i = 0; i < selection.length; i++) {
            selectionList.add((int) (Math.random() * 10));
        }
        System.out.println("Соритировка ArrayList-а:");
        System.out.println(selectionList.toString());
        inserting(selectionList);
        System.out.println(selectionList.toString());
    }

    /**
     * Сортирует массив чисел по возрастанию
     *
     * @param array массив элементов
     */
    private static void inserting(int[] array) {
        for (int i = 1; i < array.length; i++) {
            int temp = array[i];
            int in = i;
            while (in > 0 && array[in - 1] >= temp) {
                array[in] = array[in - 1];
                in--;
            }
            array[in] = temp;
        }
    }

    /**
     * Сортирует массив чисел по возрастанию
     *
     * @param selectionList массив чисел
     */
    private static void inserting(ArrayList<Integer> selectionList) {
        int length = selectionList.size();
        for (int i = 1; i < length; i++) {
            int temp = selectionList.get(i);
            int in = i;
            while (in > 0 && selectionList.get(in - 1) >= temp) {
                selectionList.set(in, selectionList.get(in - 1));
                in--;
            }
            selectionList.set(in, temp);
        }
    }
}