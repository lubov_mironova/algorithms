package ru.mla.person;

/**
 * Класс для представления людей
 *
 * @author Mironova L.
 */
public class Person {
    private String surname;
    private int year;

    public Person(String surname, int year) {
        this.surname = surname;
        this.year = year;
    }

    public Person() {
        this("Иванов", 1980);
    }

    public String getSurname() {
        return surname;
    }

    public int getYear() {
        return year;
    }

    @Override
    public String toString() {
        return "Person{" +
                "Фамилия='" + surname + '\'' +
                ", Год рождения=" + year +
                '}';
    }
}