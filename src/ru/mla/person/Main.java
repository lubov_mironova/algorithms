package ru.mla.person;

import java.util.Scanner;
import java.util.ArrayList;

/**
 * Класс для представления сортировок массива объектов
 *
 * @author Mironova L.
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Person person1 = new Person("Смирнов", 1987);
        Person person2 = new Person("Попов", 2001);
        Person person3 = new Person("Попов", 1996);
        Person person4 = new Person("Васильев", 1999);
        Person person5 = new Person("Захаров", 1979);
        Person[] people = {person1, person2, person3, person4, person5};
        Person[] people1 = {person1, person2, person3, person4, person5};
        Person[] people2 = {person1, person2, person3, person4, person5};
        out(people);
        System.out.println();
        System.out.println("Сортировка выбором по алфавиту:");
        sorting(people);
        out(people);
        System.out.println();
        System.out.println("Сортировка выбором цифр:");
        sorting2(people);
        out(people);
        System.out.println();

        System.out.println("Сортировка пузырьком по алфавиту:");
        bubbleSorting(people1);
        out(people1);
        System.out.println();
        System.out.println("Сортировка пузырьком цифр:");
        bubbleSorting2(people1);
        out(people1);
        System.out.println();

        System.out.println("Сортировка вставками цифр:");
        inserting(people2);
        out(people2);

        System.out.print("Введите фамилию для поиска:");
        String person = scanner.nextLine();
        findOutput(people, person);
    }

    /**
     * Сортирует выбором массив объектов по алфавиту
     *
     * @param people массив объектов
     */
    private static void sorting(Person[] people) {
        for (int i = 0; i < people.length - 1; i++) {
            int min = i;
            for (int j = i + 1; j < people.length; j++) {
                int isCompare = people[min].getSurname().compareTo(people[j].getSurname());
                if (isCompare > 0) {
                    min = j;
                }
            }
            if (i != min) {
                Person temp = people[i];
                people[i] = people[min];
                people[min] = temp;
            }
        }
    }

    /**
     * Сортирует пузырьком массив объектов по алфавиту
     *
     * @param people массив объектов
     */
    private static void bubbleSorting(Person[] people) {
        for (int i = 0; i < people.length; i++) {
            for (int j = i + 1; j < people.length; j++) {
                int isCompare = people[i].getSurname().compareTo(people[j].getSurname());
                if (isCompare > 0) {
                    Person temp = people[i];
                    people[i] = people[j];
                    people[j] = temp;
                    continue;
                }
            }
        }
    }

    /**
     * Сортируетмассив объектов по годам рождения по убыванию
     *
     * @param people массив объектов
     */
    private static void inserting(Person[] people) {
        for (int i = 1; i < people.length; i++) {
            Person temp = people[i];

            int in = i;
            while (in > 0 && people[in - 1].getYear() <= temp.getYear()) {
                people[in] = people[in - 1];
                in--;
            }
            people[in] = temp;
        }
    }

    /**
     * Сортирует выбором массив объектов по годам рождения по убыванию
     *
     * @param people массив объектов
     */
    private static void sorting2(Person[] people) {
        for (int i = 0; i < people.length - 1; i++) {
            int max = people[i].getYear();
            int maxI = i;
            for (int j = i + 1; j < people.length; j++) {
                if (max < people[j].getYear()) {
                    max = people[j].getYear();
                    maxI = j;
                }
            }
            Person first = people[i];
            people[i] = people[maxI];
            people[maxI] = first;
        }
    }

    /**
     * Сортирует пузырьком массив объектов по годам рождения по убыванию
     *
     * @param people массив объектов
     */
    private static void bubbleSorting2(Person[] people) {
        for (int i = 0; i < people.length; i++) {
            for (int j = i + 1; j < people.length; j++)
                if (people[i].getYear() < people[j].getYear()) {
                    Person temp = people[i];
                    people[i] = people[j];
                    people[j] = temp;
                    continue;
                }
        }
    }

    /**
     * Ищет введенную фамилию среди массива объектов
     *
     * @param people массив объектов
     * @param person введенная с консоли фамилия
     * @return массив найденных фамилий (если их несколько)
     */
    private static ArrayList<Person> find(Person[] people, String person) {
        ArrayList<Person> personName = new ArrayList<>();
        for (int i = 0; i < people.length; i++) {
            if (person.equalsIgnoreCase(people[i].getSurname())) {
                personName.add(people[i]);
            }
        }
        return personName;
    }

    /**
     * Выводит найденных людей
     *
     * @param people массив объектов
     * @param person введенная с консоли фамилия
     */
    private static void findOutput(Person[] people, String person) {
        ArrayList<Person> personName = find(people, person);
        int size = personName.size();
        if (personName.isEmpty()) {
            System.out.println("Такой фамилии нет");
        } else {
            for (int i = 0; i < size; i++) {
                System.out.println(personName.get(i) + " ");
            }
        }
    }

    /**
     * Выводит массив объектов
     *
     * @param people массив объектов
     */
    private static void out(Person[] people) {
        for (int i = 0; i < people.length; i++) {
            System.out.println(people[i] + " ");
        }
    }
}