package ru.mla.algorithmsComplexityAnalysis;

import java.util.Scanner;

public class AlgorithmsAnalysis {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n, x, y, z;
//A
        //B
        System.out.println("Введите число");
        n = scanner.nextInt();
        x = 1;
        //B
        //C
        while (x < n) {
            //D
            y = x;
            //D
            //E
            while (y <= n) {
                //F
                z = y;
                //F
                //G
                while (z <= n) {
                    //H
                    if ((z <= x * 2) && (y <= x * 2) && (z * z == x * x + y * y)) {
                        //I
                        System.out.println(x + " " + y + " " + z);
                        //I
                    }
                    //H
                    //J
                    z++;
                    //J
                }
                //G
                //K
                y++;
                //K
            }
            //E
            //L
            x++;
            //L
        }
        //C
//A
    }
}