package ru.mla.binarySearch;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

/**
 * Класс для представления бинарного поиска чисел
 *
 * @author Mironova L.
 */
public class BinarySearch {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int[] array = new int[15];

        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * 15);
        }

        System.out.print("Какое число вы хотите найти?: ");
        int key = scanner.nextInt();

        System.out.println();
        System.out.println("Array:");
        System.out.println(Arrays.toString(array));
        Arrays.sort(array);
        System.out.println(Arrays.toString(array));

        if (binarySearch0(array, key) == -1) {
            System.out.println("Число не найдено");
        } else {
            System.out.println("число: " + key + ", индекс: " + binarySearch0(array, key));
        }

        ArrayList<Integer> listArray = new ArrayList<>();
        for (int i = 0; i < 15; i++) {
            listArray.add((int) (Math.random() * 15));
        }

        System.out.println();
        System.out.println("ArrayList:");
        System.out.println(listArray.toString());
        Collections.sort(listArray);
        System.out.println(listArray.toString());

        if (binarySearch0(listArray, key) == -1) {
            System.out.println("Число не найдено");
        } else {
            System.out.println("число: " + key + ", индекс: " + binarySearch0(listArray, key));
        }
    }

    /**
     * Находит индекс введенного числа
     *
     * @param array массив чисел
     * @param key   введенное с консоли число
     * @return индекс числа в массиве
     */
    private static int binarySearch0(int[] array, int key) {
        int low = 0;
        int high = array.length - 1;

        while (low <= high) {
            int mid = (low + high) / 2;
            int midVal = array[mid];
            if (midVal == key) {
                return mid;
            }

            if (midVal < key) {
                low = mid + 1;
            } else {
                high = mid - 1;
            }
        }
        return -1;
    }

    /**
     * Находит индекс введенного числа
     *
     * @param listArray массив чисел
     * @param key       введенное с консоли число
     * @return индекс числа в массиве
     */
    private static int binarySearch0(ArrayList<Integer> listArray, int key) {
        int low = 0;
        int high = listArray.size() - 1;

        while (low <= high) {
            int mid = (low + high) / 2;
            int midVal = listArray.get(mid);
            if (midVal == key) {
                return mid;
            }

            if (midVal < key) {
                low = mid + 1;
            } else {
                high = mid - 1;
            }
        }
        return -1;
    }
}