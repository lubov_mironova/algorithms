package ru.mla.sortBySelection;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Класс для представления работы алгоритма сортировки выбором
 *
 * @author Mironova L.
 */
public class SortBySelection {
    public static void main(String[] args) {
        int[] selection = new int[9];

        for (int i = 0; i < selection.length; i++) {
            selection[i] = (int) (Math.random() * 10);
        }
        System.out.println("Сортировка массива:");
        System.out.println(Arrays.toString(selection));
        sorting(selection);
        System.out.println(Arrays.toString(selection));
        System.out.println();

        ArrayList<Integer> selectionList = new ArrayList<>();
        for (int i = 0; i < selection.length; i++) {
            selectionList.add((int) (Math.random() * 10));
        }
        System.out.println("Соритировка ArrayList-а:");
        System.out.println(selectionList.toString());
        sorting(selectionList);
        System.out.println(selectionList.toString());
    }

    /**
     * Сортирует массив чисел по возрастанию
     *
     * @param selection массив чисел
     */
    public static void sorting(int[] selection) {
        for (int i = 0; i < selection.length - 1; i++) {
            int min = selection[i];
            int minI = i;
            for (int j = i + 1; j < selection.length; j++) {
                if (min > selection[j]) {
                    min = selection[j];
                    minI = j;
                }
            }
            int first = selection[i];
            selection[i] = selection[minI];
            selection[minI] = first;
        }
    }

    /**
     * Сортирует массив чисел по возрастанию
     *
     * @param selectionList массив чисел
     */
    private static void sorting(ArrayList<Integer> selectionList) {
        for (int i = 0; i < selectionList.size() - 1; i++) {
            int minI = i;
            for (int j = i + 1; j < selectionList.size(); j++) {
                if (selectionList.get(minI) > selectionList.get(j)) {
                    minI = j;
                }
            }
            int first = selectionList.get(i);
            selectionList.set(i, selectionList.get(minI));
            selectionList.set(minI, first);
        }
    }
}
