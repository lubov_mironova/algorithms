package ru.mla.bubbleSort;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Класс для представления работы алгоритма сортировки пузырьками
 *
 * @author Mironova L.
 */
public class BubbleSort {
    public static void main(String[] args) {
        int[] selection = new int[9];

        for (int i = 0; i < selection.length; i++) {
            selection[i] = (int) (Math.random() * 10);
        }

        System.out.println("Сортировка массива:");
        System.out.println(Arrays.toString(selection));
        bubble(selection);
        System.out.println(Arrays.toString(selection));
        System.out.println();

        ArrayList<Integer> selectionList = new ArrayList<>();
        for (int i = 0; i < selection.length; i++) {
            selectionList.add((int) (Math.random() * 10));
        }
        System.out.println("Соритировка ArrayList-а:");
        System.out.println(selectionList.toString());
        bubble(selectionList);
        System.out.println(selectionList.toString());
    }


    /**
     * Сортирует массив чисел по возрастанию
     *
     * @param selection массив чисел
     */
    private static void bubble(int[] selection) {
        for (int i = selection.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++)
                if (selection[j] > selection[j + 1]) {
                    int tmp = selection[j];
                    selection[j] = selection[j + 1];
                    selection[j + 1] = tmp;
                }
        }
    }

    /**
     * Сортирует массив чисел по возрастанию
     *
     * @param selectionList массив чисел
     */
    private static void bubble(ArrayList<Integer> selectionList) {
        for (int i = selectionList.size() - 1; i > 0; i--) {
            for (int j = 0; j < i; j++)
                if (selectionList.get(j) > selectionList.get(j + 1)) {
                    int tmp = selectionList.get(j);
                    selectionList.set(j, selectionList.get(j + 1));
                    selectionList.set(j + 1, tmp);
                }
        }
    }
}
